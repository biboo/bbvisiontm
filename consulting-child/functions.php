<?php

add_action( 'wp_enqueue_scripts', 'consulting_child_enqueue_parent_styles_and_script' );

function consulting_child_enqueue_parent_styles_and_script() {
	//Style
	wp_enqueue_style( 'consulting-style', get_template_directory_uri() . '/style.css', array( 'bootstrap' ), CONSULTING_THEME_VERSION, 'all' );
	wp_enqueue_style( 'child-style', get_stylesheet_uri(), array('consulting-style') );
	// Script
	wp_enqueue_script( 'consulting-js-smoothscroll', get_template_directory_uri() . '/js/smoothscroll.js', array( 'jquery' ), CONSULTING_THEME_VERSION, 'all' );
	wp_enqueue_script( 'consulting-js-custom', get_template_directory_uri() . '/js/custom.js', array( 'jquery' ), CONSULTING_THEME_VERSION, 'all' );

}
